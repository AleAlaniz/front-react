import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-12" style={{"background-color":"red"}}>
          <form>
            <div className="form-group offset-md-3 col-md-6">
              <input className="form-control" type="search" placeholder="Insert text" />
              <button className="btn btn-primary">Send</button>
            </div>
          </form>
        </div>
      </div>

      <div className="row pt-3" style={{"background-color":"#d9d9d9","padding-bottom":"6em"}}>
          <div className="offset-md-2 col-md-8" style={{"background-color":"white","height":"30em"}}>
            <form>
              <h4 className="pt-4">
                Results:
              </h4>
              <div className="form-group">
                <input className="form-control mb-2 offset-md-2 col-md-8" placeholder="Third text" />
                <input className="form-control mb-2 offset-md-2 col-md-8" placeholder="Second text" />
                <input className="form-control mb-2 offset-md-2 col-md-8" placeholder="First text" />
              </div>
            </form>
          </div>
        </div>
    </div>
  );
}

export default App;
